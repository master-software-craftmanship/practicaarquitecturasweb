package es.urjc.code.daw.tablonanuncios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;


@Controller
public class TablonController {

	@Autowired
	private PedidoRepository repository;

	public TablonController() {
	}

	@PostConstruct
	public void init() {
		//repository.save(new Pedido("Pepe", Arrays.asList(new Elemento("sup1", false), new Elemento("sup2", false), new Elemento("sup3", false))));
		//repository.save(new Pedido("Juan", Arrays.asList(new Elemento("sup4", false), new Elemento("sup5", false), new Elemento("sup6", false))));
	}

	@GetMapping("/")
	public String tablon(Model model) {
		model.addAttribute("pedidos", repository.findAll());
		return "tablon";
	}

	@GetMapping("/pedido/nuevo")
	public String nuevoPedido(Model model) {
		Pedido pedido = new Pedido("", Collections.singletonList(new Elemento("", false)));
		model.addAttribute("pedido", pedido);
		model.addAttribute("id", "nuevo");
		model.addAttribute("pageTitle", "Nuevo Pedido");
		return "pedido";
	}

	@PostMapping("/pedido/nuevo")
	public String nuevoPedido(Model model, Pedido pedido) {
		repository.save(pedido);
		return "pedido_guardado";
	}

	@GetMapping("/pedido/{id:\\d+}")
	public String verPedido(Model model, @PathVariable long id) {
		Optional<Pedido> pedidoOptional = repository.findById(id);
		return pedidoOptional.map(pedido -> getTemplatePedidoExistente(model, pedido)).orElse("pedido_error");
	}

	@PutMapping("/pedido/{id}")
	public String modificarPedido(Model model, @PathVariable long id, Pedido pedido) {
		Optional<Pedido> pedidoOptional = repository.findById(id);
		if (pedidoOptional.isPresent()) {
			repository.save(pedido);
			return getTemplatePedidoExistente(model, pedido);
		} else {
			model.addAttribute("pedidos", repository.findAll());
			return "tablon";
		}
	}

	@DeleteMapping("/pedido/{id}")
	public String borrarPedido(Model model, @PathVariable long id) {
		Optional<Pedido> pedidoOptional = repository.findById(id);
		pedidoOptional.ifPresent(pedido -> repository.delete(pedido));
		model.addAttribute("pedidos", repository.findAll());
		return "tablon";
	}

	private String getTemplatePedidoExistente(Model model, Pedido pedido) {
		model.addAttribute("pedido", pedido);
		model.addAttribute("id", pedido.getId());
		model.addAttribute("isEditarPedido", true);
		model.addAttribute("pageTitle", "Pedido " + pedido.getTitulo());
		return "pedido";
	}
}