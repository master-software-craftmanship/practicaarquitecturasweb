package es.urjc.code.daw.tablonanuncios;

import javax.persistence.Embeddable;

@Embeddable
public class Elemento {
    private String key;
    private boolean value;

    public Elemento() {
        this.key = "";
        this.value = false;
    }

    public Elemento(String key, boolean value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Elemento{" +
                "key='" + key + '\'' +
                ", value=" + value +
                '}';
    }

}
