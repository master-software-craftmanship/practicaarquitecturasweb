package es.urjc.code.daw.tablonanuncios;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Pedido {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String titulo;

	@ElementCollection
	private List<Elemento> elementos;

	public Pedido() {
		id = -1;
		titulo = "";
		elementos = new ArrayList<>();
	}

	public Pedido(String titulo, List<Elemento> elementos) {
		super();
		this.id = -1;
		this.titulo = titulo;
		this.elementos = elementos;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<Elemento> getElementos() {
		return elementos;
	}

	public void setElementos(List<Elemento> elementos) {
		this.elementos = elementos;
	}
	
	private String elementosToString() {
		StringBuilder output = new StringBuilder();
		for (Elemento elemento : elementos) {
			output.append(elemento.toString()).append(", ");
		}
		return output.substring(0, output.length() - 2);
	}

	@Override
	public String toString() {
		return "Pedido{titulo='" + titulo + "', elementos=[" + elementosToString() + "]}";
	}
}
