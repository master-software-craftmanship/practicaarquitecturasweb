$(document).ready(function(){
    $("#mainNav").addClass("navbar-shrink");
    activateAutomaticFormValidation();
    $("#listaElementos .botonEliminarElemento").on("click", processRemoveElementoClick);
    $("#nuevoElemento").button().on("click", function(){
        createNewElementoPedido();
        enablePedidosRemoveButton();
    });
    $("#removePedidoButton").on("click", function(){
        jQuery.ajax({
            url: $(this).attr("data-url-pedido"),
            type: 'DELETE',
            success: function(http) {
                document.open();
                document.write(http);
                document.close();
                window.history.pushState(null, "Tablon", "/");
            }
        });
    });
    $("#editarPedido").button().on("click", setEditPedidoMode);
    if($("#viewModeOn").length == 0) {
        setNewPedidoMode();
    } else {
        setViewPedidoMode();
    }
    initCheckboxesTachado();
});

function setNewPedidoMode() {
    hideElementosCheckbox();
    setEditModeOn();
    hideViewPedidoButtons();
    showEditButtons();
}

function setViewPedidoMode() {
    hideElementosCheckbox();
    $("#formPedidos :input").attr("disabled", true);
    setEditModeOff();
    hideEditButtons();
    showViewPedidoButtons();
}

function setEditPedidoMode() {
    setNewPedidoMode();
    showElementosCheckbox();
}

function showElementosCheckbox() {
    $(".elementos_value_checkbox").removeClass("d-none");
}

function hideElementosCheckbox() {
    $(".elementos_value_checkbox").addClass("d-none");
}

function showEditButtons() {
    $("#nuevoElementoDiv").removeClass("d-none");
    $("#salvarPedido").removeClass("d-none");
    $("#nuevoElementoDiv").attr("disabled", false);
    $("#salvarPedido").attr("disabled", false);
}

function hideEditButtons() {
    $("#nuevoElementoDiv").addClass("d-none");
    $("#salvarPedido").addClass("d-none");
}

function showViewPedidoButtons() {
    $("#editarPedido").removeClass("d-none");
    $("#borrarPedido").removeClass("d-none");
    $("#editarPedido").removeClass("d-none");
    $("#editarPedido").attr("disabled", false);
    $("#borrarPedido").attr("disabled", false);
    $("#editarPedido").attr("disabled", false);
}

function hideViewPedidoButtons() {
    $("#editarPedido").addClass("d-none");
    $("#borrarPedido").addClass("d-none");
    $("#editarPedido").addClass("d-none");
}

function activateAutomaticFormValidation() {
    // Credits to https://getbootstrap.com/docs/4.0/components/forms/#validation
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
          setElementosNamesAndStrike();
        }
        form.classList.add('was-validated');
      }, false);
    });
}

function setEditModeOn() {
    setEditMode(true);
    enablePedidosRemoveButton();
}

function setEditModeOff() {
    setEditMode(false);
    disablePedidosRemoveButtonUnconditionally();
}

function setEditMode(value) {
    $("#formPedidos :input").attr("disabled", !value);
}

function processRemoveElementoClick(event) {
    removeElementoPedido(event.target);
    disablePedidosRemoveButton();
}

function createNewElementoPedido() {
    $(`
    <div class="col-4">
        <div class="feature-item">
            <label>
                <div>Elemento:</div>
                <div class="form-check-inline elementos_container">
                    <input type="checkbox" class="form-control elementos elementos_value elementos_value_checkbox d-none" name="elementos[].value" value="true">
                    <input type="hidden" name="elementos[].value" value="false" />
                    <input type="text" class="row form-control elementos elementos_key" name="elementos[].key" required>
                    <div class="row invalid-feedback">
                        El elemento no puede estar vacio.
                    </div>
                </div>
            </label>
        </div>
        <a class="d-none btn btn-outline btn-s botonEliminarElemento">Borrar</a>
    </div>
   `).insertBefore("#nuevoElementoDiv").children(".botonEliminarElemento").on("click", processRemoveElementoClick);
   initCheckboxesTachado();
};

function enablePedidosRemoveButton() {
    if($("#listaElementos").children().length > 2) {
        $("#listaElementos .botonEliminarElemento").removeClass("d-none");
    }
}

function disablePedidosRemoveButton() {
    if($("#listaElementos").children().length < 3) {
        disablePedidosRemoveButtonUnconditionally();
    }
}

function disablePedidosRemoveButtonUnconditionally() {
    $("#listaElementos .botonEliminarElemento").addClass("d-none");
}

function removeElementoPedido(botonBorrar) {
    botonBorrar.parentElement.remove();
}

function setElementosNamesAndStrike() {
    $("#formPedidos .elementos_container").each(function(index) {
        let checkboxitem =  $(this).children(".elementos_value");
        checkboxitem.attr("name", "elementos[" + index + "].value");
        let elementoChecked = checkboxitem.is(':checked');
        let keyitem =$(this).children(".elementos_key");
        keyitem.attr("name", "elementos[" + index + "].key");
        if (elementoChecked) {
            keyitem.addClass("tachado");
        } else {
            keyitem.removeClass("tachado");
        }
        $(this).children('input[type="hidden"]').attr("name", "elementos[" + index + "].value");
    });
}

function initCheckboxesTachado() {
    $(".elementos_value").on("click", setElementosNamesAndStrike);
    setElementosNamesAndStrike();
}
